<?php

require_once('Adapter.php');

class Ice_Db_PdoMySqlAdapter extends Ice_Db_Adapter {

    protected $pdo;

    public function load($obj) {
        $query = "SELECT * FROM `" . $obj->getTable() . "` WHERE `" . $obj->getKey() . "` = ?" . $obj->getKey();
        $stmt = $this->executeSelect($query, $obj);
        $rs = $stmt->fetchAll(PDO::FETCH_CLASS, get_class($obj));
        if (count($rs) >= 1) {
            $row = $rs[0];
            foreach ($row as $key => $value) {
                $obj->$key = $value;
            }
            return true;
        }
        return false;
    }

    public function query($obj, $query) {
        $stmt = $this->executeSelect($query, $obj);
        return $stmt->fetchAll(PDO::FETCH_CLASS, get_class($obj));
    }

    public function update($obj, $fields, $where) {
        $fieldArr = explode(",", $fields);
        $builder = "UPDATE `" . $obj->getTable() . "` SET ";
        foreach ($fieldArr as $key => $f) {
            $f = trim($f);
            if (!strstr($f, " "))
                $f .= " = ?" . $f;
            $builder .= $f;
            if ($key < count($fieldArr) - 1) {
                $builder .= " , ";
            }
        }

        if (empty($where)) {
            $where = $obj->getKey() . " = ?" . $obj->getKey();
        }

        $builder .= " WHERE " . $where;
        return $this->executeUpdate($builder, $obj);
    }

    public function select($obj, $where, $choice, $order, $group, $pageIndex, $pageSize) {
        $list = array();
        if (empty($choice))
            $choice = "*";
        $query = "SELECT " . $choice . " FROM " . $obj->getTable();
        if (!empty($where))
            $query .= " WHERE " . $where;
        if (!empty($group))
            $query .= " GROUP BY " . $group;
        if (!empty($order))
            $query .= " ORDER BY " . $order;
        if ($pageIndex >= 0 && $pageSize > 0)
            $query .= " LIMIT " . ($pageIndex * $pageSize) . "," . $pageSize;
		
        $stmt = $this->executeSelect($query, $obj);
        return $stmt->fetchAll(PDO::FETCH_CLASS, get_class($obj));
    }

    public function insert($obj, $fields) {
        $f = "(";
        $v = "( ";
        $option = explode(",", $fields);
        foreach ($option as $i => $value) {
            $value = trim($value);
            $f .= "`" . $value . "`";
            $v .= "?" . $value;
            if ($i < (count($option) - 1)) {
                $f .= ",";
                $v .= " , ";
            } else {
                $f .= ")";
                $v .= " )";
            }
        }
		$query = "INSERT INTO `" . $obj->getTable() . "` " . $f . " VALUES " . $v;
        return $this->executeInsert($query, $obj);
    }

    public function delete($obj, $where) {
        if (empty($where)) {
            $where = $obj->getKey() . " = ?" . $obj->getKey();
        }
        return $this->executeUpdate("DELETE FROM `" . $obj->getTable() . "` WHERE " . $where, $obj);
    }

    public function setup($host, $port, $dbName, $username, $password) {
        $this->pdo = new PDO('mysql:dbname=' . $dbName . ';host=' . $host . ';port=' . $port . ';charset=utf8', $username, $password, array(
        	PDO::MYSQL_ATTR_INIT_COMMAND => 'set names utf8'
		));
    }

    public function prepare($query) {
        return $this->pdo->prepare($query);
    }

    public function getLastInsertId() {
        return $this->pdo->lastInsertId();
    }

}

?>