<?php
ini_set ( 'default_charset', 'UTF-8' );
class CommonUtils {
	
	private static $uKey = array ("A", "D", "E", "I", "O", "U", "Y", "a", "d", "e", "i", "o", "u", "y" );
	
	private static $uVal = array (array ("À", "Á", "Ả", "Ã", "Ạ", "Ă", "Ằ", "Ắ", "Ẵ", "Ẳ", "Ặ", "Â", "Ầ", "Ấ", "Ẫ", "Ẩ", "Ậ" ), array ("Đ" ), array ("È", "É", "Ẻ", "Ẽ", "Ẹ", "Ê", "Ề", "Ế", "Ể", "Ễ", "Ệ" ), array ("Ì", "Í", "Ỉ", "Ĩ", "Ị" ), array ("Ò", "Ó", "Ỏ", "Õ", "Ọ", "Ô", "Ồ", "Ố", "Ỗ", "Ổ", "Ộ", "Ơ", "Ờ", "Ớ", "Ở", "Ỡ", "Ợ" ), array ("Ù", "Ú", "Ủ", "Ũ", "Ụ", "Ư", "Ừ", "Ứ", "Ử", "Ữ", "Ự" ), array ("Ỳ", "Ý", "Ỷ", "Ỹ", "Ỵ" ), array ("à", "á", "ả", "ã", "ạ", "â", "ầ", "ấ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ" ), array ("đ" ), array ("é", "è", "ẻ", "ẽ", "ẹ", "ê", "ề", "ế", "ể", "ễ", "ệ" ), array ("ì", "í", "ỉ", "ĩ", "ị" ), array ("ò", "ó", "ỏ", "õ", "ọ", "ô", "ồ", "ố", "ổ", "ỗ", "ộ", "ơ", "ờ", "ớ", "ở", "ỡ", "ợ" ), array ("ù", "ú", "ủ", "ũ", "ụ", "ư", "ừ", "ứ", "ử", "ữ", "ự" ), array ("ỳ", "ý", "ỷ", "ỹ", "ỵ" ) );
	
	private static $map = array ("!", "@", "#", "$", "%", "^", "&", "*", "(", ")" );
	
	private static $thresHold = 10;
	
	private static $converter = null;
	
	public static function getDateMonthConverter() {
		if (CommonUtils::$converter == null) {
			CommonUtils::$converter = array ();
			CommonUtils::$converter ['Jan'] = '01';
			CommonUtils::$converter ['Feb'] = '02';
			CommonUtils::$converter ['Mar'] = '03';
			CommonUtils::$converter ['Apr'] = '04';
			CommonUtils::$converter ['May'] = '05';
			CommonUtils::$converter ['Jun'] = '06';
			CommonUtils::$converter ['Jul'] = '07';
			CommonUtils::$converter ['Aug'] = '08';
			CommonUtils::$converter ['Sep'] = '09';
			CommonUtils::$converter ['Oct'] = '10';
			CommonUtils::$converter ['Nov'] = '11';
			CommonUtils::$converter ['Dec'] = '12';
			
			CommonUtils::$converter ['January'] = '01';
			CommonUtils::$converter ['February'] = '02';
			CommonUtils::$converter ['March'] = '03';
			CommonUtils::$converter ['April'] = '04';
			CommonUtils::$converter ['May'] = '05';
			CommonUtils::$converter ['June'] = '06';
			CommonUtils::$converter ['July'] = '07';
			CommonUtils::$converter ['August'] = '08';
			CommonUtils::$converter ['September'] = '09';
			CommonUtils::$converter ['October'] = '10';
			CommonUtils::$converter ['November'] = '11';
			CommonUtils::$converter ['December'] = '12';
			
			CommonUtils::$converter ['01'] = 'Jan';
			CommonUtils::$converter ['02'] = 'Feb';
			CommonUtils::$converter ['03'] = 'Mar';
			CommonUtils::$converter ['04'] = 'Apr';
			CommonUtils::$converter ['05'] = 'May';
			CommonUtils::$converter ['06'] = 'Jun';
			CommonUtils::$converter ['07'] = 'Jul';
			CommonUtils::$converter ['08'] = 'Aug';
			CommonUtils::$converter ['09'] = 'Sep';
			CommonUtils::$converter ['10'] = 'Oct';
			CommonUtils::$converter ['11'] = 'Nov';
			CommonUtils::$converter ['12'] = 'Dec';
		}
		return CommonUtils::$converter;
	}
	
	public static function customizedDistance($src, $des) {
		$totalLen = mb_strlen ( $src . $des );
		if ($totalLen == 0) {
			return - 1;
		}
		$srcTmp = CommonUtils::convertFromUtf8ToAscii ( $src );
		$desTmp = CommonUtils::convertFromUtf8ToAscii ( $des );
		$srcTmp = CommonUtils::convertStringWeight ( $srcTmp );
		$desTmp = CommonUtils::convertStringWeight ( $desTmp );
		$totalLen = mb_strlen ( $srcTmp . $desTmp );
		return levenshtein ( $srcTmp, $desTmp ) / $totalLen;
	}
	
	private static function convertStringWeight($input) {
		$output = "";
		$chars = CommonUtils::mb_str_split ( $input );
		foreach ( $chars as $char ) {
			if (is_numeric ( $char )) {
				$addedChars = "";
				for($j = 0; $j < CommonUtils::$thresHold; $j ++) {
					$addedChars .= CommonUtils::$map [$char];
				}
				$output .= $addedChars;
			} else {
				$output .= $char;
			}
		}
		return $output;
	}
	
	public static function normalizeTerm($term) {
		$output = CommonUtils::convertFromUtf8ToAscii ( $term );
		$output = strtolower ( $output );
		$output = preg_replace ( "/[^a-z0-9]/", "", $output );
		return $output;
	}
	
	public static function convertFromUtf8ToAscii($input) {
		$output = $input;
		$arrLen = count ( CommonUtils::$uVal );
		for($j = 0; $j < $arrLen; $j ++) {
			$output = str_replace ( CommonUtils::$uVal [$j], CommonUtils::$uKey [$j], $output );
		}
		return $output;
	}
	
	public static function charAt($string, $index) {
		if ($index < mb_strlen ( $string )) {
			return mb_substr ( $string, $index, 1 );
		} else {
			return - 1;
		}
	}
	
	public static function mb_str_split($str, $length = 1) {
		if ($length < 1)
			return FALSE;
		$result = array ();
		for($i = 0; $i < mb_strlen ( $str ); $i += $length) {
			$result [] = mb_substr ( $str, $i, $length );
		}
		return $result;
	}
	
	public static function encodeJSON($object) {
		return Zend_Json::encode ( $object );
	}
	
	public static function encodeXML($object, $rootNodeName) {
		//first convert $object into array
		$data = self::objectToArray ( $object );
		$xml = simplexml_load_string ( "<?xml version='1.0' encoding='utf-8'?><$rootNodeName/>" );
		//convert array to xml
		return self::arrayToXML ( $data, $xml )->asXML ();
	}
	
	public static function arrayToXML($data, $xml) {
		foreach ( $data as $k => $v ) {
			if (is_numeric ( $k ))
				$k = 'number-' . $k;
			$k = preg_replace ( '/[^a-z]/i', '', $k );
			is_array ( $v ) ? self::arrayToXML ( $v, $xml->addChild ( $k ) ) : $xml->addChild ( $k, $v );
		}
		return $xml;
	}
	
	public static function objectToArray($object) {
		if (! is_object ( $object ) && ! is_array ( $object )) {
			return $object;
		}
		if (is_object ( $object )) {
			$object = get_object_vars ( $object );
		}
		return array_map ( 'self::objectToArray', $object );
	}
	
	public static function normalizeString($string) {
		//TODO: Implement this function!
		$string = str_replace("'", "", $string);
		return $string;
	}
	
	public static function classNameToPath($className) {
		$classFile = str_replace ( '_', DIRECTORY_SEPARATOR, $className ) . '.php';
		return $classFile;
	}
	
	public static function appendBasePath($path, $className) {
		$basePrefix = str_replace ( DIRECTORY_SEPARATOR, '_', $path );
		return $basePrefix . $className;
	}
	
	public static function createObjectFromClass($className, $needIncludeFile = true, $incFile = null, $params=null) {
		if ($needIncludeFile) {
			if ($incFile == null) {
				$incFile = self::classNameToPath ( $className );
			}
			if (Zend_Loader::isReadable ( $incFile )) {
				include_once $incFile;
			}
		}
		$object = new $className ($params);
		return $object;
	}
	
	/**
	 * Call a method dynamically
	 * @param String $className the class name, or the object 	
	 * @param String $functionName the method name, maybe static or not
	 * @param array $params the parameters to be passed to the function
	 * @param $incFile the file to be included, containing the class
	 */
	public static function callFunction($className, $functionName, $params = array(), $incFile = null) {
		//NOTICES: If you allow users (even admin) to specify the $incFile (via HTTP 
		//request params) then you MUST verify it first. Beware of the Directory 
		//Traversal attack
		if (! is_object ( $className )) {
			if ($incFile == null) {
				$incFile = self::classNameToPath ( $className );
			}
			if (Zend_Loader::isReadable ( $incFile )) {
				include_once $incFile;
			}
		}
		$result = call_user_func_array ( array ($className, $functionName ), $params );
		return $result;
	}
	
	public static function keywordToShowOnFriendPage(){
		$result = array('name','image_link');
		return $result;
	}

	public static function getSuggestAddNewString($keyword){
		$lang = "vi";
		$langList = array("en"=>"Add new ","vi"=>"Thêm mới ");
		// chang keyword
		return $langList[$lang]." $keyword";
	}
	
	public static function buildMessageFromErrorCode($error)	{
		switch ($error)	{
			case GlobalConfig::LOGIN_INIT_ERROR_THEFT:
				return Message::LOGIN_COOKIE_THEFT;
			default:
				return null;
		}
	}
	
	public static function getKeywordLang()	{
		$lang = "Vi";
		return "keywordsContent".$lang;
	}
	
	public static function upperFirstChar($string)	{
		$string = strtoupper(substr($string, 0, 1)).substr($string, 1);
		return $string;
	}
	public static function validateEmail($email){
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}
	
	public static function generatePassword($params){
		return sha1($params);
	}
	
	public static function friendlyLink($s) {
		if (!isset($s)) {
			return;
		}
		$newclean = $s;
		$uni1 = 'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|A';
		$arr1 = explode($uni1, "|");
		for($i=0; $i<count($uni1);$i++){
			$newclean = str_replace($uni1[$i],'a',$newclean);
		}
		$uni1 = "è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|E";
		$arr1 = explode($uni1, "|");
		for($i=0; $i<count($uni1);$i++){
			$newclean = str_replace($uni1[$i],'e',$newclean);
		}
		$uni1 = 'ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ|I';
		$arr1 = explode($uni1, "|");
		for($i=0; $i<count($uni1);$i++){
			$newclean = str_replace($uni1[$i],'i',$newclean);
		}
		$uni1 = 'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|O';
		$arr1 = explode($uni1, "|");
		for($i=0; $i<count($uni1);$i++){
			$newclean = str_replace($uni1[$i],'o',$newclean);
		}
		$uni1 = 'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|U';
		$arr1 = explode($uni1, "|");
		for($i=0; $i<count($uni1);$i++){
			$newclean = str_replace($uni1[$i],'u',$newclean);
		}
		$uni1 = 'ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ|Y';
		$arr1 = explode($uni1, "|");
		for($i=0; $i<count($uni1);$i++){
			$newclean = str_replace($uni1[$i],'y',$newclean);
		}
		$uni1 = 'd|Đ|D';
		$arr1 = explode($uni1, "|");
		for($i=0; $i<count($uni1);$i++){
			$newclean = str_replace($uni1[$i],'d',$newclean);
		}
		$newclean = strtolower($newclean);
		$newclean = str_replace('/[\&]/g', '-va-',$newclean);
		$newclean = str_replace('/[^a-zA-Z0-9._-]/g', '-',$newclean);
		$newclean = str_replace('/[-]+/g', '-',$newclean);
		$newclean = str_replace('/-$/', '',$newclean);
		return $newclean;
	} 
}


class EmailUtils {
	public static function initMail(){
		$smtpConnection = new Zend_Mail_Transport_Smtp(EmailConfig::$host, EmailConfig::$config);
		Zend_Mail::setDefaultTransport($smtpConnection);
	}
	public static function sendMail($to, $subject, $content){
		$mail = new Zend_Mail();
		$mail->setBodyText($content);
		$mail->setFrom('noreply@joolist.com', 'Picture Something');
		$mail->type = 'text/html';
		$mail->addTo($to);
		$mail->setSubject($subject);
		$mail->send();
	}
}
