<?php

require_once ('configs/PerspectiveConfig.php');

class DBUtils {

    private static $instance = null;
    public static function getDb(){
        return self::$instance;
    }
    public static function createDatabaseAdapter() {
        if (self::$instance == null)
            self::$instance = Zend_Db::factory('Pdo_Mysql', array('host' => DbConfig::$host, 'username' => DbConfig::$username, 'password' => DbConfig::$passwd, 'dbname' => DbConfig::$dbName, 'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'utf8\'')));
        return self::$instance;
    }

    public static function mysqlEscape($string) {
        mysql_connect(DbConfig::$host, DbConfig::$username, DbConfig::$passwd);
        return mysql_real_escape_string($string);
    }

    public static function buildOrQuery($col, $oids) {
        mysql_connect(DbConfig::$host, DbConfig::$username, DbConfig::$passwd);
        $s = '';
        foreach ($oids as $oid) {
            $oid = mysql_real_escape_string($oid);
            $s = $s . " $col = '" . $oid . "' or";
        }
        $s = rtrim($s, 'or');
        return $s;
    }

    public static function checkLangSuggestion($lang) {
        if ($lang != "en" && $lang != "vi") {
            $lang = "en";
        }
        return $lang;
    }

}

class UserAuth {

    /**
     * @param unknown_type $username
     * @param unknown_type $passwd
     */
    public static function AuthenticateUser($username, $passwd, $storageSession = 'userAuth') {
        $auth = Zend_Auth::getInstance();
        //create a new session storage
        $auth->setStorage(new Zend_Auth_Storage_Session($storageSession));
        $db = DBUtils::createDatabaseAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($db);
        $authAdapter->setTableName('users')
                ->setIdentityColumn('username')
                ->setCredentialColumn('password')
                ->setCredentialTreatment('SHA1(?)');
        //accept user input
        $authAdapter->setIdentity($username);
        $authAdapter->setCredential($passwd);
        //perform authentication
        $result = $auth->authenticate($authAdapter);
        if ($result->isValid()) {
            $namespace = new Zend_Session_Namespace($storageSession);
            $namespace->username = $username;
            $namespace->setExpirationSeconds(GlobalConfig::$loginSessionExpireTime);
            return true;
        }
        return false;
    }

}

class Utils {

    public static function EncryptedPassword($pass) {
        $passwordkey = '0';
        return sha1($pass);
    }

    public static function createUploadedFileName($key) {
        $passwordkey = "this.is.an.incredible.long.sentence.used.for.creating.uploaded.filename.in.bkprofile";
        $key = $key . $passwordkey . rand();
        return sha1($key);
    }

    public static function createActivationCode($key) {
        $passwordkey = "this.is.an.incredible.long.sentence.used.for.creating.activation.code.in.bkprofile";
        $time = time();
        $key = $key . $passwordkey . $time;
        return sha1($key);
    }

    public static function createInvitationCode($key) {
        $passwordkey = "this.is.an.incredible.long.sentence.used.for.creating.invitation.code.in.bkprofile";
        $time = time();
        $key = $key . $passwordkey . $time;
        return sha1($key);
    }

    public static function createUniqueCode($prefix = 'a') {
        $passwordkey = "this.is.an.incredible.long.sentence.used.for.creating.unique.code.in.bkprofile";
        $time = time();
        $key = $passwordkey . $time;
        return $prefix . sha1($key);
    }

    /**
     * 
     * Create login serie ID for persistent login
     * Login serie ID, together with user ID are unique for each login attempt
     */
    public static function createPL_UserSerieID($initial = '') {
        $passwordkey = "this.is.an.incredible.long.sentence.used.for.creating.pl.login.serie.id.in.bkprofile";
        $time = time();
        $rand = rand();
        $key = $initial . $passwordkey . $time . $rand;
        return sha1($key);
    }

    /**
     * 
     * Creating one-time token for persistent login
     * @param unknown_type $initial
     */
    public static function createPL_LoginTokenID($initial = '') {
        $passwordkey = "this.is.an.incredible.long.sentence.used.for.creating.pl.login.token.id.in.bkprofile";
        $rand = rand();
        $key = $initial . $passwordkey . $rand;
        return sha1($key);
    }

    public static function createPasswordRecoverToken($id) {
        $passwordkey = "this.is.an.incredible.long.sentence.used.for.creating.password.recovery.token.in.bkprofile";
        $rand = rand();
        $key = $passwordkey . $rand . $id . time();
        return sha1($key);
    }

}

?>