<?php

abstract class BaseWebController extends Zend_Controller_Action {

    /*
    protected $user;
    protected $permitted;
    protected $browserCache = true;
    public $result;

    public function purify() {
    }

    protected function purifyElement($purifier, $value) {
        if (is_object($value)) {
            return $value;
        }
        if (is_array($value)) {
            foreach ($value as $key => $v) {
                $value [$key] = $this->purifyElement($purifier, $v);
            }
        } else {
            $value = $purifier->purify($value);
        }
        return $value;
    }

    public function init() {
        $this->purify();
        $plugin = $this->getFrontController()->getPlugin(
                'Zend_Controller_Plugin_ErrorHandler');
        if ($plugin != null) {
            $plugin->setErrorHandler(
                    array('controller' => 'ajax-error', 'action' => 'error'));
        }
    }
    
    public function isPermitted() {
        return $this->permitted;
    }

    public function getUser() {
        return $this->user;
    }

    public function preDispatch() {
    	$this->scriptPath = $this->view->getScriptPath();
    }

    public function setupUser($uid) {
        if ($uid) {
            $user = new User();
            $user->id = $uid;
            $user->load();
            $this->user = $user;
            $this->view->user = $user;
        } else {
            $user = new User();
            if (isset($_COOKIE['tracking_login']))
                $this->user = new User(); //$user->getUserFromToken($_COOKIE['tracking_login']);
        }
    }

    public function checkPermission($throws = true) {
        $this->permitted = false;
        if ($this->getUser() != null) {
            $permit = $this->permit($this->getUser());
        } else {
            $permit = $this->defaultPermission();
        }
        if (!$permit) {
            $this->permitted = false;
            if ($throws) {
                require_once ('Exceptions/PermissionDeniedException.php');
                throw new PermissionDeniedException ();
            } else {
                $this->_forward('login', 'user', null, array('error' => "You don't have permission to take this action. Please login"));
            }
            return false;
        }
        $this->permitted = true;
        return true;
    }

    public function permit($user) {
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
        $permission = new Permission(null);
        return $permission->check($user->role, $controller, $action);
    }

    public function defaultPermission() {
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
        $permission = new Permission(null);
        return $permission->check('default', $controller, $action);
    }

    public function hasInputParam($param) {
        if (!$this->_hasParam($param))
            return false;
        $p = $this->getRequest()->getParam($param);
        if ($p === '')
            return false;
        return true;
    }

    public function userHasLogin() {
        return ($this->user != null);
    }

    public function destroySession() {
        Zend_Session::destroy(true);
        $this->user = null;
    }
    */
}

?>
