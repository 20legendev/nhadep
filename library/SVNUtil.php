<?php
    class SVN {
    	public static function createRepo($svn) {
    		exec('svnadmin create /var/svn/'.$svn);
    	}
		
		public static function checkout($svn, $deployPath) {
			if (!file_exists('/var/www/'.$deployPath)) {
				mkdir('/var/www/'.$deployPath, true);
			}
			exec('svn checkout http://localhost/svn/'.$svn.' /var/www/'.$deployPath.'/'.$svn);
		}
		
		public static function update($svn, $deployPath) {
			if (!file_exists('/var/www/'.$deployPath.'/'.$svn)) {
				self::checkout($svn, $deployPath);
			}
			exec('svn update /var/www/'.$deployPath.'/'.$svn);
		}
    }
?>