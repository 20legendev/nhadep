-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 23, 2015 at 07:28 AM
-- Server version: 5.5.41-0ubuntu0.14.10.1
-- PHP Version: 5.5.12-2ubuntu4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nhadep`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_imageupload`
--

CREATE TABLE IF NOT EXISTS `admin_imageupload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `link` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=62 ;

--
-- Dumping data for table `admin_imageupload`
--

INSERT INTO `admin_imageupload` (`id`, `user_id`, `link`) VALUES
(2, 8, 'public/media/20140205/20140205032927000000.png'),
(3, 8, 'public/media/20140205/20140205033050000000.png'),
(4, 8, 'public/media/20140205/20140205033130000000.png'),
(5, 8, 'public/media/20140205/20140205033141000000.png'),
(6, 8, 'public/media/20140205/20140205041729000000.jpg'),
(7, 8, 'public/media/20140205/20140205073050000000.jpg'),
(8, 8, 'public/media/20140205/20140205073640000000.jpg'),
(9, 8, 'public/media/20140205/20140205073650000000.jpg'),
(10, 8, 'public/media/20140205/20140205073731000000.jpg'),
(11, 8, 'public/media/20140205/20140205074410000000.jpg'),
(12, 8, 'public/media/20140205/20140205074414000000.jpg'),
(13, 8, 'public/media/20140205/20140205074418000000.jpg'),
(14, 8, 'public/media/20140205/20140205074450000000.jpg'),
(15, 8, 'public/media/20140205/20140205074455000000.jpg'),
(16, 8, 'public/media/20140205/20140205074503000000.jpg'),
(17, 8, 'public/media/20140205/20140205111145000000.jpg'),
(18, 8, 'public/media/20140205/20140205111149000000.jpg'),
(19, 8, 'public/media/20140205/20140205111240000000.jpg'),
(20, 8, 'public/media/20140205/20140205111244000000.jpg'),
(21, 8, 'public/media/20140205/20140205111248000000.jpg'),
(22, 8, 'public/media/20140205/20140205111338000000.jpg'),
(23, 8, 'public/media/20140205/20140205111343000000.jpg'),
(24, 8, 'public/media/20140205/20140205111346000000.jpg'),
(25, 8, 'public/media/20140205/20140205111420000000.jpg'),
(26, 8, 'public/media/20140205/20140205111428000000.jpg'),
(27, 8, 'public/media/20140205/20140205111431000000.jpg'),
(28, 8, 'public/media/20140205/20140205111452000000.jpg'),
(29, 8, 'public/media/20140206/20140206084830000000.jpg'),
(30, 8, 'public/media/20140206/20140206084840000000.jpg'),
(31, 8, 'public/media/20140206/20140206084840000000.jpg'),
(32, 8, 'public/media/20140206/20140206084840000000.jpg'),
(33, 8, 'public/media/20140206/20140206084919000000.jpg'),
(34, 8, 'public/media/20140206/20140206084931000000.jpg'),
(35, 8, 'public/media/20140206/20140206084936000000.jpg'),
(36, 8, 'public/media/20140206/20140206085001000000.jpg'),
(37, 8, 'public/media/20140206/20140206085008000000.jpg'),
(38, 8, 'public/media/20140206/20140206085025000000.jpg'),
(39, 8, 'public/media/20140206/20140206085044000000.jpg'),
(40, 8, 'public/media/20140206/20140206085104000000.jpg'),
(41, 8, 'public/media/20140206/20140206085125000000.jpg'),
(42, 8, 'public/media/20140206/20140206085134000000.jpg'),
(43, 8, 'public/media/20140206/20140206085150000000.jpg'),
(44, 8, 'public/media/20140206/20140206085155000000.jpg'),
(45, 8, 'public/media/20140206/20140206085200000000.jpg'),
(46, 8, 'public/media/20140206/20140206085222000000.jpg'),
(47, 8, 'public/media/20140206/20140206085227000000.jpg'),
(48, 8, 'public/media/20140206/20140206085234000000.jpg'),
(49, 8, 'public/media/20140206/20140206085315000000.jpg'),
(50, 8, 'public/media/20140206/20140206092939000000.jpg'),
(51, 8, 'public/media/20140206/20140206093038000000.jpg'),
(52, 8, 'public/media/20140206/20140206093059000000.jpg'),
(53, 8, 'public/media/20140208/20140208083641000000.jpg'),
(54, 8, 'public/media/20140208/20140208083719000000.jpg'),
(55, 8, 'public/media/20140208/20140208083755000000.jpg'),
(56, 8, 'public/media/20140208/20140208083915000000.jpg'),
(57, 8, 'public/media/20140208/20140208083945000000.jpg'),
(58, 8, 'public/media/20140213/20140213100009000000.jpg'),
(59, 8, 'public/media/20140213/20140213100026000000.jpg'),
(60, 8, 'public/media/20140213/20140213100035000000.jpg'),
(61, 8, 'public/media/20140213/20140213100107000000.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `userid` int(11) DEFAULT '1',
  `since` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `hit` int(11) DEFAULT '0',
  `comment` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `name`, `description`, `content`, `userid`, `since`, `hit`, `comment`) VALUES
(1, 'Đơn giản mà đẹp 75m² cho gia đình trẻ', 'Gia đình 4 người sống thật thoải mái trong không gian nội thất tối giản nhưng không hề đơn điệu.', 'Căn hộ tuyệt đẹp này là nơi ở của gia đình bốn người. Nội thất của căn hộ rất đơn giản và rõ nét, đạt độ cân đối hoàn hảo. Những bức tường được sơn trắng toàn bộ và vật liệu gỗ được sử dụng chủ đạo tạo cảm giác ấm cùng và hấp dẫn cho ngôi nhà.<div>Một mặt, trang trí nội thất khá sơ sài, tối giản với một bảng màu giới hạn và không có quá nhiều họa tiết hoặc kết cấu phức tạp. Mặt khác, các yếu tố gỗ tự nhiên khiến căn phòng cảm thấy ấm áp và đầy chào đón.<br></div>', 1, '2014-02-05 04:18:39', 0, 0),
(2, '20 mẫu thiết kế phòng ngủ cho bé trai', '', '', 1, '2014-02-05 04:34:59', 0, 0),
(3, ' Sử dụng màu sắc cho căn phòng của bé', 'Căn phòng được trang trí rực rỡ và thú vị nhất chắc hẳn phải là phòng riêng của bé. Ngay cả khi những khu vực còn lại của ngôi nhà bạn mang chút vẻ ảm đạm. Hãy cùng sáng tạo nên căn phòng tuyệt vời nhất dành cho bé yêu', '<br>', 1, '2014-02-05 04:40:13', 0, 0),
(4, '3 giải pháp phân vùng không gian sống nhỏ không cần vách ngăn', 'Đối với không gian sống nhỏ, nhiều người không muốn dùng tường hay vách ngăn trong để hạn chế sự bí bách. Vậy đâu là giải pháp để vừa giữ được sự thoáng đãng, vừa phân chia được các không gian trong phòng?', '<b>\n			 \n			 Những chiếc thảm phân định không gian!</b><div><br></div><div>Sử dụng thảm là ý tưởng thông minh để vừa phân chia rạch ròi nhưng vẫn có sự gắn kết hài hòa giữa 2 vùng không gian chức năng trong cùng một phòng với nhau. Trên thị trường, có nhiều loại thảm đa dạng về màu sắc, chất liệu để vừa tạo nên sự cá tính, vừa đảm bảo tính thẩm mỹ của toàn bộ căn phòng.		</div>', 1, '2014-02-05 04:41:13', 0, 0),
(5, 'Nhà 22m2 siêu tiện nghi của mẹ đơn thân', 'Tuy có diện tích siêu nhỏ nhưng căn hộ của bà mẹ trẻ và con gái nhỏ lại tiện nghi và ấm cùng một cách khó tin.', '\n			 Một căn hộ có diện tích 22m2 là rất nhỏ bé và về mặt lý thuyết thì gần như không có đủ không gian dành cho một gia đình. Tuy nhiên, với trí óc sáng tạo thì bạn có thể biến nó trở nên hữu ích hơn. Mặc dù rất nhỏ nhưng căn hộ có phương án bài trí thú vị, thực sự tiện nghi và khá ấm cúng. Bất chấp sự thiếu hụt về không gian và thực tế các chức năng khác nhau cùng nằm trong một căn phòng thì nơi này không hề tạo cảm giác chật chội, gò bó hay lộn xộn.Hãy cùng xem những hạn chế về không gian được giải quyết như thế nào trong trường hợp này. Khu vực ngủ và không gian lưu trữ được kết hợp trong một cấu trúc hình hộp. Phía trên dùng làm giường ngủ và bên dưới có một số phòng để lưu trữ. Và đây không phải là ý tưởng thông minh và linh hoạt duy nhất được sử dụng tại đây. Cầu thang cung cấp đường đi tới khu vực giường treo và còn hữu dụng hơn thế. Mỗi bậc cầu thang là một ngăn kéo dùng để cất gọn mọi loại đồ đạc trong nhà.Gian bếp nhỏ và là một phần của thiết kế mở nhưng không hề thiếu chỗ để thao tác. Một bề mặt có thể nâng lên và để bồn rửa bát bên dưới. Thực sự là một ý tưởng thiết kế thiết thực. Chỗ lưu trữ ẩn có mặt ở khắp mọi nơi. Và đồ nội thất đa chức năng là điều tuyệt vời khác về căn hộ này, ví dụ như bàn ăn có thể hạ thấp xuống để làm bàn cà phê.		', 1, '2014-02-05 04:41:33', 0, 0),
(6, 'Nhà 29m2 bày đơn giản vẫn dễ say mê', 'Thật khó tin căn hộ đẹp tuyệt và tiện nghi này chỉ có vỏn vẹn 29m2.', '\n			 \n			 \n			 \n			 \n			 29m2 thực sự không có nhiều không gian, thậm chí không bằng một căn hộ studio. Mọi người dễ cảm thấy một không gian như vậy rất nhỏ bé và chật chội. Tuy nhiên, mọi thứ có thể hoàn toàn ngược lại nếu bạn chọn một thiết kế nội thất thông minh. Hãy lấy căn hộ này làm ví dụ vì thực tế nó còn nhỏ hơn rất nhiều so với những hình ảnh dưới đây.										', 1, '2014-02-05 04:42:45', 0, 0),
(7, 'Bài trí thật chuẩn cho phòng khách dài và hẹp', 'Chúng ta hãy cùng nghiên cứu một số ví dụ về phòng khách có hình dáng dài và hẹp, xem cách chủ nhân bài trí phòng khách trở nên đẹp và lôi cuốn hơn', 'Hầu hết, hình dáng của căn phòng quyết định thiết kế nội thất và phong cách trang trí. Nếu một phòng hình chữ nhật và rất rộng thì cách trang trí cần bù đắp được điều này. Tuy nhiên, nếu căn phòng dài và hẹp thì vấn đề trở nên thử thách hơn nhiều.\n			 		', 1, '2014-02-13 10:01:13', 0, 0),
(8, NULL, NULL, NULL, 1, '2014-02-13 10:01:16', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `article_image`
--

CREATE TABLE IF NOT EXISTS `article_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` text COLLATE utf8_unicode_ci,
  `image_link` text COLLATE utf8_unicode_ci,
  `image_desc` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=146 ;

--
-- Dumping data for table `article_image`
--

INSERT INTO `article_image` (`id`, `article_id`, `image_link`, `image_desc`) VALUES
(2, '0', 'public/media/20140205/20140205041729000000.jpg', '\n						addddd					'),
(3, '0', 'public/images/default-admin.png', ''),
(4, '0', 'public/media/20140205/20140205041729000000.jpg', '\n						addddd					'),
(5, '0', 'public/media/20140205/20140205041729000000.jpg', '\n						addddd					'),
(6, '0', 'public/media/20140205/20140205041729000000.jpg', '\n						addddd					'),
(7, '0', 'public/media/20140205/20140205041729000000.jpg', '\n						Căn hộ gây ấn tượng bởi phong cách nội thất linh hoạt và quyến rũ với hai gam màu chủ đạo là đen và trắng, kết hợp các chi tiết màu nâu và xám rất hài hòa.					'),
(8, '0', 'public/media/20140205/20140205041729000000.jpg', '\n						addddd					'),
(13, '6', 'public/media/20140205/20140205041729000000.jpg', '\n						\n						\n						Căn hộ gây ấn tượng bởi phong cách nội thất linh hoạt và quyến rũ với hai gam màu chủ đạo là đen và trắng, kết hợp các chi tiết màu nâu và xám rất hài hòa.&nbsp;<br>Như bạn thấy, phòng khách, phòng ngủ, phòng ăn và bàn làm việc đều được bố trí trong cùng một khu vực. Dĩ nhiên, thông thường thì bạn sẽ cần một căn phòng khổng lồ để chứa đầy đủ những chức năng này. Nhưng không, bạn vẫn có thể làm được điều này nếu sắp xếp mọi thứ một cách linh hoạt.<br>Trong căn hộ này, phòng ngủ thực sự được nâng lên khỏi sàn nhà để tiết kiệm diện tích sàn nhà. Bên dưới nó được sử dụng để thiết kế không gian làm việc.<br>										'),
(14, '6', 'public/media/20140205/20140205073050000000.jpg', 'Tường nhà và trần nhà được sơn màu trắng, kết hợp bức tường kính rộng lớn khiến cho căn phòng trông rộng hơn nhiều so với diện tích thực.<br>Phương án nâng cao giường ngủ tạo ra nhiều không gian hơn cho khu vực ghế ngồi và thậm chí là bàn ăn. Mặc dù bị hạn chế bởi diện tích quá nhỏ nhưng việc kết hợp các không gian chức năng với nhau lại không hề khó khăn. Tất cả đều được giới hạn rất đẹp và thay vì làm cho căn phòng trở nên nhỏ bé là lộn xộn thì chúng thật sự làm căn phòng có vẻ đầy chức năng và thực tế hơn.<br>'),
(17, '5', 'public/media/20140205/20140205073640000000.jpg', '"Thiên đường sống" của hai mẹ con gói gọn trong 22m2.'),
(18, '5', 'public/media/20140205/20140205073650000000.jpg', 'Giường ngủ và tủ đựng đồ được thiết kế kết hợp với nhau trong cấu trúc một chiếc hộp.'),
(19, '5', 'public/media/20140205/20140205073731000000.jpg', 'Việc tiếp cận giường ngủ treo cực kỳ dễ dàng nhờ một chiếc cầu thang làm bằng gỗ.'),
(56, '4', 'public/media/20140205/20140205074410000000.jpg', 'Bên cạnh đó, bạn nên tìm hiểu kỹ về màu sắc, chất liệu thảm trước khi mua sao cho phù hợp nhất với khí hậu và không gian sống của mình. Ví dụ, thời tiết nóng ẩm của Việt Nam không hợp với thảm len, thảm lông. Thảm một màu và mặt trơn hợp nhất với đồ nội thất hiện đại, còn thảm họa tiết vừa trẻ trung, vừa giúp tạo điểm nhấn cho không gian.\n											'),
(57, '4', 'public/media/20140205/20140205074414000000.jpg', 'Thảm một màu và mặt trơn hợp nhất với đồ nội thất hiện đại.\n											'),
(58, '4', 'public/media/20140205/20140205074418000000.jpg', '<b><br>Bức tường và những điểm nhấn</b><br>Những bức tường quanh nhà sẽ trở nên xinh đẹp hơn khi được trang trí, dù chỉ là trang trí nhẹ nhàng. Không những vậy nếu biết khéo léo sắp đặt, trang trí nó còn giúp tạo ra điểm nhấn phân chia ranh giới các khu vực chức năng.Căn phòng dưới đây không rộng rãi lắm nhưng có đến 3 không gian khác nhau được phân tách bằng tranh: phòng khách với bộ tranh 5 bức to, góc thư giãn đơn giản hơn với 2 bức nhỏ xinh và phòng ăn chỉ cần một bức đủ lớn để nội bật lên vị trí của nó... Màu sơn be nhã nhặn và vài bức tranh đựợc sắp đặt có chủ ý trên tường cạnh vị trí mỗi nơi đã hoàn thành tốt sự phân chia khu vực của chủ nhà.\n											'),
(59, '4', 'public/media/20140205/20140205074450000000.jpg', 'Trên thị trường hiện có khá nhiều mẫu giấy dán tường hay đề can trang trí đẹp mắt, và việc sử dụng chúng để phân chia không gian là hoàn toàn có thể. Ví dụ góc bàn ăn dưới đây đã nổi bật và tách bạch khỏi không gian chung nhờ mảng giấy dán tường họa tiết hoa dây nhẹ nhàng.\n											'),
(60, '4', 'public/media/20140205/20140205074455000000.jpg', '<b><br>Sử dụng ánh sáng. </b><br>Nếu là người có đam mê với màu sáng và ánh sáng chắc hẳn bạn sẽ rất ấn tượng với ý tưởng phân chia không gian bằng ánh sáng. Cụ thể là sử dụng các loại đèn, thậm chí là cách màu đèn khác nhau cho từng khu vực. Căn phòng dưới đây lại được phân chia bằng đèn. Cụ thể với không gian tiếp khách là chiếc đèn trần lớn, không gian bàn ăn là chiếc đèn nhỏ, còn phòng góc nghỉ là chiếc đèn đọc sách ấm áp và xinh xắn.\n											'),
(61, '4', 'public/media/20140205/20140205074503000000.jpg', 'Phân chia không gian thông minh bằng ánh sáng.\n											'),
(92, '1', 'public/media/20140205/20140205111145000000.jpg', 'Những chiếc ghế ngồi màu đen của khu vực bàn ăn thực sự bắt mắt. Chúng đẹp đẽ, nổi bật nhờ màu sắc tương phản.&nbsp;<br>Thêm vào đó, thiết kế đơn giản, mạch lạc làm cho chúng trông giống như được vẽ bằng bút chì sắc nét.<br>Lối trang trí xuyên suốt căn hộ có sự thống nhất. Tất cả các phòng đều được ốp sàn gỗ và điều này tạo nên diện mạo tự nhiên và giản dị.'),
(93, '1', 'public/media/20140205/20140205111149000000.jpg', 'Bàn ăn đặt đối diện với gian bếp'),
(94, '1', 'public/media/20140205/20140205111240000000.jpg', 'Các chi tiết trong phòng bếp đều đơn giản nhưng sắc nét, đặc biệt là vòi nước mạ vàng nổi bật'),
(95, '1', 'public/media/20140205/20140205111244000000.jpg', 'Cách trang trí tối giản, bảo toàn nét mộc mạc, tự nhiên của vật liệu gỗ chủ đạo'),
(96, '1', 'public/media/20140205/20140205111248000000.jpg', 'Chậu cây chuỗi ngọc tăng thêm màu sắc cho bàn ăn. Ngoài ra còn có thêm một số đồ chơi bằng gỗ nhỏ xinh cho hai cô bé nghịch trong bữa ăn.Chức năng đóng một vai trò quan trọng trong việc thiết kế không gian này. Đây là lý do vì sao gỗ xuất hiện ở khắp mọi nơi. <br>Những cây cảnh xanh tươi bổ sung cho gỗ và tăng thêm màu sắc cho các phòng.Căn hộ cũng có thêm những màu sắc rực rỡ khác dưới dạng các chi tiết trang trí tạo điểm nhấn.&nbsp;<br>'),
(97, '1', 'public/media/20140205/20140205111338000000.jpg', '<span style="text-align: center;">Phòng khách siêu tối giản chỉ với một chiếc sofa dài màu xám. Đây không là nơi vui chơi dành cho các con nhỏ</span>'),
(98, '1', 'public/media/20140205/20140205111343000000.jpg', 'Nơi chơi đùa của trẻ nhỏ được phân định bởi tấm thảm len ấm áp và chiếc tủ ly đựng đồ giản dị'),
(99, '1', 'public/media/20140205/20140205111346000000.jpg', 'Phòng ngủ vợ chồng đối diện phòng khách, tuy nhỏ nhưng được bài trí rất thông minh và đẹp mắt'),
(100, '1', 'public/media/20140205/20140205111420000000.jpg', 'Phòng ngủ dành cho con nằm bên cạnh phòng ngủ bố mẹ'),
(101, '1', 'public/media/20140205/20140205111428000000.jpg', 'Phòng tắm tương phản với toàn bộ không gian còn lại trong nhà do cách phối màu. Các chi tiết nổi bật mạ vàng thật sự cuốn hút.&nbsp;Phòng tắm trắng thời thượng nhờ các thiết bị mạ vàng sáng hút mắt'),
(102, '1', 'public/media/20140205/20140205111431000000.jpg', 'Phòng sinh hoạt chung nhìn từ ngoài cửa chính vào'),
(103, '1', 'public/media/20140205/20140205111452000000.jpg', 'Gác xép nhỏ được thiết kế ngay phía trên hành lang vừa là không gian nghỉ cho khách, vừa được tận dụng làm nơi lưu trữ đồ tiện lợi'),
(104, '3', 'public/media/20140206/20140206084919000000.jpg', 'Vàng là một màu sắc phù hợp với mọi giới tính cũng như làm sinh động không gian của bé'),
(105, '3', 'public/media/20140206/20140206084931000000.jpg', ''),
(106, '3', 'public/media/20140206/20140206084936000000.jpg', ''),
(107, '3', 'public/media/20140206/20140206085001000000.jpg', ''),
(108, '3', 'public/media/20140206/20140206085008000000.jpg', ''),
(109, '3', 'public/media/20140206/20140206085044000000.jpg', ''),
(110, '3', 'public/media/20140206/20140206085025000000.jpg', ''),
(111, '3', 'public/media/20140206/20140206085104000000.jpg', ''),
(112, '3', 'public/media/20140206/20140206085125000000.jpg', ''),
(113, '3', 'public/media/20140206/20140206085134000000.jpg', ''),
(114, '3', 'public/media/20140206/20140206085150000000.jpg', ''),
(115, '3', 'public/media/20140206/20140206085155000000.jpg', ''),
(116, '3', 'public/media/20140206/20140206085200000000.jpg', ''),
(117, '3', 'public/media/20140206/20140206085222000000.jpg', ''),
(118, '3', 'public/media/20140206/20140206085227000000.jpg', ''),
(119, '3', 'public/media/20140206/20140206085234000000.jpg', ''),
(120, '3', 'public/media/20140206/20140206085315000000.jpg', ''),
(127, '2', 'public/media/20140206/20140206092939000000.jpg', 'Khung tranh hình Tin Tin và chăn bông hình cao bồi mang đến phong cách mạnh mẽ trong phòng ngủ. Điều đặc biệt là với bức tường trắng sẽ dễ dàng giúp bạn trang trí khi bé lớn lên. Chiếc túi treo tường sẽ giúp bé giữ phòng ngủ luôn gọn gàng và chiếc tủ đựng quần áo là ngăn chứa tuyệt vời.'),
(128, '2', 'public/media/20140206/20140206093038000000.jpg', '<b>2. Giấy dán tường hình quân nhân </b><br>Giấy dán tường hình xe bus kết hợp với quân nhân cùng với lá cờ Union Jack (cờ của Vương quốc Anh) mang đến cảm giác vui tươi cho căn phòng'),
(129, '2', 'public/media/20140206/20140206093059000000.jpg', '<b>3. Phòng ngủ kẻ sọc </b><br>Hãy mang đến không gian thoải mái cho bé bằng những đường kẻ sọc trên sàn phòng ngủ. Bạn có thể tạo ra những đường kẻ bằng những màu sắc cùng tông để tạo nên hiệu ứng.'),
(134, '8', 'public/media/20140213/20140213100009000000.jpg', '<div style="text-align: justify;">Phòng khách này chia sẻ một thiết kế bán mở với phòng ăn. Nhưng chỉ riêng khu vực phòng khách vẫn rất dài và hẹp. Điều này có nghĩa rằng một khu vực ghế ngồi duy nhất sẽ rất khó để tạo ra. Thay vào đó là thiết kế hai khu vực ghế ngồi khác nhau. Về cơ bản, cả hai khu vực đều sử dụng ghế sofa và bàn cà phê nhưng chúng không hề xung đột mà bổ sung cho nhau</div>'),
(135, '8', 'public/media/20140213/20140213100026000000.jpg', 'Trong trường hợp này, giải pháp lại hoàn toàn khác. Thay vì ngăn chia căn phòng và người sử dụng ở trong hai không gian khác nhau thì một khu vực ghế ngồi duy nhất đã được sắp xếp. Một phần khác của phòng khách tích hợp thêm bức tường thấp, hình chữ L với thiết kế nhỏ gọn và hiện đại. Ngoài ra, không còn điều gì cản trở việc trang trí và căn phòng có diện mạo thoáng mát và rộng rãi.'),
(136, '8', 'public/media/20140213/20140213100035000000.jpg', 'Ở đây, chúng ta có thêm phương án chọn nhiều hơn một chiếc ghế sofa và ghế bành. Bây giờ, nhìn phòng khách này thậm chí không thấy nó hẹp nhờ được lấp đầy đồ nội thất ấm cúng. Ghế sofa được bố trí ở giữa phòng và xung quanh là ghế bành, ghế ottoman và các chi tiết khác. Đây là một khu vực ghế ngồi được mở rộng và bảng màu chủ yếu tập trung vào tông màu đất, góp phần tạo nên bầu không khí ấm áp và thoải mái cho căn phòng.'),
(137, '8', 'public/media/20140213/20140213100107000000.jpg', 'Một cách khách để đối phó với căn phòng dài và hẹp là sắp xếp đồ nội thất vuông góc với chiều dài của căn phòng. Ví dụ trong phòng khách này, hai chiếc ghế ngồi đáng yêu được đặt ở hướng khác nhau và xoay quanh một chiếc bàn tròn. Trên bức tường ngăn chia tiếp giáp với căn phòng có một chiếc lò sưởi làm bầu không khí căn phòng thoải mái hơn. Một bức tường khách bị chiếm đóng bởi một chiếc sofa dài ấm áp.'),
(142, '7', 'public/media/20140213/20140213100009000000.jpg', '\n						<div style="text-align: justify;">Phòng khách này chia sẻ một thiết kế bán mở với phòng ăn. Nhưng chỉ riêng khu vực phòng khách vẫn rất dài và hẹp. Điều này có nghĩa rằng một khu vực ghế ngồi duy nhất sẽ rất khó để tạo ra. Thay vào đó là thiết kế hai khu vực ghế ngồi khác nhau. Về cơ bản, cả hai khu vực đều sử dụng ghế sofa và bàn cà phê nhưng chúng không hề xung đột mà bổ sung cho nhau</div>					'),
(143, '7', 'public/media/20140213/20140213100026000000.jpg', '\n						Trong trường hợp này, giải pháp lại hoàn toàn khác. Thay vì ngăn chia căn phòng và người sử dụng ở trong hai không gian khác nhau thì một khu vực ghế ngồi duy nhất đã được sắp xếp. Một phần khác của phòng khách tích hợp thêm bức tường thấp, hình chữ L với thiết kế nhỏ gọn và hiện đại. Ngoài ra, không còn điều gì cản trở việc trang trí và căn phòng có diện mạo thoáng mát và rộng rãi.					'),
(144, '7', 'public/media/20140213/20140213100035000000.jpg', '\n						Ở đây, chúng ta có thêm phương án chọn nhiều hơn một chiếc ghế sofa và ghế bành. Bây giờ, nhìn phòng khách này thậm chí không thấy nó hẹp nhờ được lấp đầy đồ nội thất ấm cúng. Ghế sofa được bố trí ở giữa phòng và xung quanh là ghế bành, ghế ottoman và các chi tiết khác. Đây là một khu vực ghế ngồi được mở rộng và bảng màu chủ yếu tập trung vào tông màu đất, góp phần tạo nên bầu không khí ấm áp và thoải mái cho căn phòng.					'),
(145, '7', 'public/media/20140213/20140213100107000000.jpg', '\n						Một cách khách để đối phó với căn phòng dài và hẹp là sắp xếp đồ nội thất vuông góc với chiều dài của căn phòng. Ví dụ trong phòng khách này, hai chiếc ghế ngồi đáng yêu được đặt ở hướng khác nhau và xoay quanh một chiếc bàn tròn. Trên bức tường ngăn chia tiếp giáp với căn phòng có một chiếc lò sưởi làm bầu không khí căn phòng thoải mái hơn. Một bức tường khách bị chiếm đóng bởi một chiếc sofa dài ấm áp.					');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Phòng khách'),
(2, 'Phòng trẻ em'),
(3, 'Phòng ngủ'),
(4, 'Phòng ăn'),
(5, 'Phòng tắm');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT '1',
  `title` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `category` int(11) DEFAULT '1',
  `view` int(11) DEFAULT '0',
  `like` int(11) DEFAULT '0',
  `since` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `userid`, `title`, `link`, `description`, `category`, `view`, `like`, `since`) VALUES
(1, 2, 'Gam màu sáng - tối', 'public/media/living1.jpg', NULL, 1, 0, 0, '2014-01-23 08:32:45'),
(2, 2, 'Gam màu sáng tối', 'public/media/living2.jpg', NULL, 1, 0, 0, '2014-01-23 08:40:27'),
(3, 2, 'Hiện đại và nhiều sáng', 'public/media/living3.jpg', NULL, 1, 0, 0, '2014-01-23 08:41:25'),
(4, 2, 'Ấm cúng', 'public/media/living4.jpg', NULL, 1, 0, 0, '2014-01-23 08:41:44'),
(5, 2, 'Ấm cúng', 'public/media/living5.jpg', NULL, 1, 0, 0, '2014-01-23 08:42:02'),
(6, 3, 'Ấm cúng', 'public/media/living6.jpg', NULL, 1, 0, 0, '2014-01-23 08:42:49'),
(7, 3, 'Dinning', 'public/media/dinning.jpg', NULL, 2, 0, 0, '2014-01-23 10:20:06'),
(8, 3, 'Phòng nhỏ', 'public/media/dinning1.jpg', NULL, 2, 0, 0, '2014-01-23 10:20:10'),
(9, 3, 'Kiểu cổ điển', 'public/media/dinning2.jpg', NULL, 2, 0, 0, '2014-01-23 10:20:29'),
(10, 3, 'Bàn kính', 'public/media/dinning3.jpg', NULL, 2, 0, 0, '2014-01-23 10:20:35');

-- --------------------------------------------------------

--
-- Table structure for table `imagetag`
--

CREATE TABLE IF NOT EXISTS `imagetag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imageid` int(11) DEFAULT NULL,
  `tagid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `imagetag`
--

INSERT INTO `imagetag` (`id`, `imageid`, `tagid`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 2),
(4, 3, 4),
(5, 3, 1),
(6, 4, 4),
(7, 4, 1),
(8, 5, 3),
(9, 6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `popular`
--

CREATE TABLE IF NOT EXISTS `popular` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imageid` int(11) DEFAULT NULL,
  `since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `name`) VALUES
(1, 'Cổ điển'),
(2, 'Hiện đại'),
(3, 'Quý tộc'),
(4, 'Thanh cảnh');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` text COLLATE utf8_unicode_ci,
  `displayname` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contractor_type` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `phone` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8_unicode_ci,
  `type` int(11) DEFAULT '1' COMMENT '0:admin, 1: viewer, 2: designer, 3: seller',
  `since` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `avatar`, `displayname`, `contractor_type`, `address`, `phone`, `location`, `type`, `since`) VALUES
(2, NULL, NULL, 'http://upload.wikimedia.org/wikipedia/vi/0/08/CLBB%C4%90_Ho%C3%A0ng_Anh_Gia_Lai.png', 'Nội thất Hoàng Anh', 1, NULL, NULL, NULL, 2, '2014-01-24 07:11:16'),
(3, NULL, NULL, 'http://joondesigner.com/wp-content/uploads/2012/05/dung-joon.jpg', 'Joon designer', 1, NULL, NULL, NULL, 2, '2014-01-24 07:11:34');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
