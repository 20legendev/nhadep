Utils = {
	
	init: function(){
		$('body').on('click',function(e){
			$('#userDetailPopup').remove();
		})
		this.lastView = localStorage.getItem('nhadep.view');
		if(!this.lastView){
			this.lastView = 'xl';
		}
		$('#imageListUI').addClass('view' + this.lastView);
		var _self = this;
		/*
		$(window).scroll(function(e){
			_self.scroll(e);
		})
		*/
	},
	
	scroll: function(){
		if($(window).scrollTop() > this.lastScroll){
			if(!this.hide){
				$('.second-menu').addClass('hide');
				this.hide = 1;
			}
		}else{
			if(this.hide){
				$('.second-menu').removeClass('hide');
				this.hide = 0;
			}
		}
		this.lastScroll = $(window).scrollTop();
	},
	
	openDetail: function(id){
		
	},
	
	viewStream: function(idx){
		if(!document.getElementById('streamWrapper')){
			$('body').append('<div id="streamWrapper"></div>');
		}
		wrapper = $('#streamWrapper');
		var obj = designs[idx];
		console.log(obj);
		wrapper.append($('#StreamView').html());
		$('#streamImageUI').attr('src', obj.link);
	},
	
	setView: function(view){
		localStorage.setItem('nhadep.view', view);
		if(this.lastView){
			$('#imageListUI').removeClass('view' + this.lastView);
		}
		$('#imageListUI').addClass('view' + view);
		this.lastView = view;
	},
	
	showUserPreview: function(userid, obj, e){
		e.stopPropagation();
		$('#userDetailPopup').remove();
		obj.append($('#userDetailTemplate').html());
	}
	
}
$(document).ready(function(){
	Utils.init();	
})