Admin = {
	init: function(){
		
	}
}

ImageChooser = {
	
	init: function(callback){
		this.close();
		$('body').append($('#imageChooser').html());
		this.loadMyImages();
		this.initUploader();
		this.callback = callback;
	},
	
	loadMyImages: function(){
		var _self = this;
		$.ajax({
			url: 'admin-service/load-my-image',
			success: function(data){
				_self.renderImages(JSON.parse(data).data);
			}
		})
	},
	
	renderImages: function(data){
		this.imageData = data;
		var wrapper = $('#imageContainer');
		wrapper.html('');
		var str = '';
		for(var i=0;i<data.length;i++){
			str += '<img class="image-loader-item" onclick="ImageChooser.select('+data[i].id+', $(this))" src="' + data[i].link  + '" />';
			wrapper.append(str);
			str = '';
		}
	},
	
	select: function(id, obj){
		$('.image-loader-item').removeClass('active');
		obj.addClass('active');
		var link = '';
		for(var i=0; i < this.imageData.length;i++){
			if(this.imageData[i].id == id){
				link = this.imageData[i].link;
				break;
			}
		}
		this.selected = {
			id: id,
			link: link
		}
	},
	
	done: function(){
		this.callback(this.selected);
		this.close();
	},
	
	initUploader: function(){
		this.uploader = new qq.FileUploader({
			element : document.getElementById('uploader'),
			action : "admin-service/upload-image",
			debug : false
		});
	},
	
	close: function(){
		$('#imageLoader').remove();
	}
	
}

Article = {
	
	counter: 0,
	
	edit: function(idx){
		this.editing = idx;
		var _self = this;
		ImageChooser.init(function(data){
			_self.select(data);
		})	
	},
	
	addImage: function(){
		this.counter++;
		$('#featuredZone').append('<div id="featuredImage'+this.counter+'" class="featured-image"><img src="public/images/default-admin.png" onclick="Article.edit('+this.counter+')"/><div class="article-edit-zone small" contenteditable="true" ></div><div>Hủy</div></div>');
	},
	
	select: function(data){
		$('#featuredImage' + this.editing + ' img').attr('src', data.link); 
	},
	
	publish: function(){
		var data = {
			title: $('#title').val(),
			shortContent: $('#short-content').val(),
			start: $('#editable').html(),
			images: []
		};
		var obj = $('#featuredZone .featured-image'); 
		var id;
		for(var i=0;i<obj.length;i++){
			id = $(obj[i]).attr('id');
			data.images.push({
				image: $('#' + id + ' img').attr('src'),
				content: $('#' + id + ' .article-edit-zone').html()
			});
		
		}
		var _self = this;
		$.ajax({
			url: 'admin-service/add-article',
			data: data,
			success: function(ret){
				console.log(ret);
			}
		})
	}
	
}
