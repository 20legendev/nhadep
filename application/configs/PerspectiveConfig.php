<?php

class GlobalConfig {

    static $loginSessionExpireTime = 3600;
    static $supportEmail = 'support@bkprofile.com';
    /* Should we display error verbosely ? */
    static $verbose = true;
    /* Should we display debug stack trace ? */
    static $showStackTrace = true;
    static $pageTitle = 'BKProfile - Connecting people';
    static $maxLoginAttempt = 3;
    static $blockSessionTimeout = 1800;
    static $siteName = 'Terra';
	static $statusApp = 1;
    static $PAYMENT_ENABLE = true;
    static $COUNTRY_REMOTE_US = 'USA';
	static $randomFactor = 512;
	static $socialUrl = "http://internal.joolist.com/demo/social-terrabook";
	static $trackUrl = "http://internal.joolist.com/demo/tracking-terrabook";
}

class ExceptionConfig {

    const NOTFOUND_EXCEPTION = "NOTFOUND_EXCEPTION";
    const UNDERCONSTRUCTION_EXCEPTION = "UNDERCONSTRUCTION_EXCEPTION";
    const UNDERMAINTENANCE_EXCEPTION = "UNDERMAINTENANCE_EXCEPTION";
    const PERM_EXCEPTION = 'PERM_EXCEPTION';
}

class DbConfig {
	/*
    static $host = "internal.joolist.com";
    static $port = "3306";
	static $username = "joolist";
    static $password = "gWaqOjd63";
    static $dbName = "joolist";
	 */
	static $host = "localhost";
    static $port = "3306";
	static $username = "root";
    static $password = "kien2014";
    static $dbName = "nhadep";
    static $adapter = "Ice_Db_PdoMySqlAdapter";
}

class EmailConfig {

    static $config = array('auth' => 'login', 'ssl' => 'ssl', 'port' => 465, 'username' => 'picturesomethingapp@gmail.com', 'password' => 'kiennguyen');
    static $host = "smtp.gmail.com";
}
class ProductConfig  {
	static $itemPerPage = 12;
}

class AppConfig {
    static $productNotFound = "Không tìm thấy sản phẩm";
}

class CollectionConfig {
	static $list = array(
		array(
			'config'=>array(
				'extclasses'=>"new-books scrollable",
            	'datasource-class'=>"GetNewAppsService",
            	'datasource-input'=>"model",
            	'datasource-item'=>"SmallBookIcon"
			)
		),
		array(
			'config'=>array(
				'layout'=>"flow",
	        	'extclasses'=>"collections",
	        	'datasource-class'=>"GetCollectionsService",
	        	'datasource-item'=>"CollectionItem",
	        	'datasource-input'=>"model"
			)
		),
		array(
			'config'=>array(
				'layout'=>"flow",
	        	'extclasses'=>"top-books scrollable",
	        	'datasource-class'=>"GetTopAppsService",
	        	'datasource-item'=>"SmallBookIcon",
	        	'datasource-input'=>"model"
			),
			'title'=>'Ứng dụng nổi bật'
		),
		array(
			'config'=>array(
				'layout'=>"flow",
				'extclasses'=>"collections other-collections scrollable",
				'datasource-class'=>"GetCollectionsService",
	        	'datasource-item'=>"SmallCollectionItem",
	        	'datasource-input'=>"model",
	        	'datasource-params'=>array('type'=>1)
			),
			'title'=>'Bộ ứng dụng cho bé'
		)
	);
}

?>
