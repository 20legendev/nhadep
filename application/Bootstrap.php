<?php

session_start();
//require_once ('HtmlPurifier/HTMLPurifier.auto.php');
require_once 'Ice/Db/AdapterFactory.php';
require_once 'configs/PerspectiveConfig.php';

/**
 * 
 * The application's starting point
 * @author griever
 *
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
	
	/**
	 * The starting point of our application, used for
	 * registering common variables
	 * (non-PHPdoc)
	 * @see library/Zend/Application/Bootstrap/Zend_Application_Bootstrap_Bootstrap::run()
	 */
	public function run() {
		$this->registerHelpers();
// 		$this->setupMail ();
		$this->setupDb();
// 		$this->setupCache();
		parent::run ();
	}
	
	public function setupCache() {
		try {
		CacheManager::$cache = Zend_Cache::factory('core',
		     'memcached',
		     array(
		     	'lifetime'=>7200
			 ),
		     array());
		} catch (Exception $ex) {
		}
	}
	
	public function setupDb() {
		Ice_Db_AdapterFactory::setupAdapter(DbConfig::$adapter, DbConfig::$host, DbConfig::$port, DbConfig::$username, DbConfig::$password, DbConfig::$dbName);
	}
	
	public function registerHelpers() {
		Zend_Controller_Action_HelperBroker::addPath ( 'controllers/Action/Helper', 'Custom_Helper' );
	}
	
	public function setupMail() {
		$config = EmailConfig::$config;
		$trans = new Zend_Mail_Transport_Smtp ( EmailConfig::$host, $config );
		Zend_Mail::setDefaultTransport ( $trans);
	}
	
	
// 	public function _initRoutes() {
// 	    $this->bootstrap('FrontController');
// 	    $this->_frontController = $this->getResource('FrontController');
// 	    $router = $this->_frontController->getRouter();
	   /*
	    $new_route = new Zend_Controller_Router_Route(
	        'nha-dep.html',
	        array(
	            'controller'=>'photos',
	            'action'=>'index'
	        )
	    );
	    $router->addRoute('photos', $new_route);
	    */
	    /*
	    $new_route = new Zend_Controller_Router_Route(
	        'y-tuong-nha-dep.html',
	        array(
	            'controller'=>'idea',
	            'action'=>'index'
	        )
	    );
	    $router->addRoute('idea', $new_route);
	    */
// 	}
	
	
}