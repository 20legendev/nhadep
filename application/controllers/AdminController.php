<?php

require_once "Base/BaseWebController.php";
require_once 'DBUtils.php';
require_once 'CommonUtils.php';
require_once 'models/Article.php';
require_once 'models/ArticleImage.php';

class AdminController extends BaseWebController {
	
	public function init(){
		$this->_helper->layout()->setLayout('admin-layout');
		parent::init();
		if(!$this->userId){
			$action = $this->getRequest()->getActionName();
			if($action != 'login'){
				$_SESSION['userid'] = 8;
				$this->userId = 8;
				// $this->_helper->redirector('login', 'admin');
			}
		}
	}
	
	public function indexAction(){
		echo "KIEN";
		return;
	}
	
	public function loginAction(){
		
	}
	
	public function articleAction(){
		$id = $this->_getParam('id');
		$article = new Article();
		$ai = new ArticleImage();
		if(isset($id)){
			$article->id = $id;
			if($article->load()){
				$_SESSION['editid'] = $id;
			}else{
				$_SESSION['editid'] = -1;
			}
			$ai->article_id = $id;
			$this->view->images = $ai->getByArticle();
			$this->view->article = $article;
			$this->_helper->viewRenderer('articleedit'); 
		}else{
			$data = $article->getAll();
			for($i=0;$i<count($data); $i++){
				$ai->article_id = $data[$i]->id;
				$data[$i]->images = $ai->getByArticle();
			}
			$this->view->articles = $data;	
		}
	}
}
