<?php
class PermissionDeniedException extends Zend_Controller_Exception {
	public function __construct() {
		parent::__construct ( "You don't have permission to perform this action" );
	}
}
?>