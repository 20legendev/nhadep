<?php

/**
 *
 * @author Griever
 * @version 
 */
require_once 'Zend/Loader/PluginLoader.php';
require_once 'Base.php';

/**
 * SendAjaxResponse Action Helper 
 * 
 * @uses actionHelper Zend_Controller_Action_Helper
 */
class Zend_Controller_Action_Helper_RenderAjaxResponse extends Dummy_Helper_Base {

    public function renderAjaxResponse() {
        $controller = $this->getActionController();

        //encode response in desired format
        $encoder = $controller->getHelper('encode');
        if ($encoder != null) {
            $controller->result = $encoder->encode(array('result' => $this->result), 'ajax-results');
        }

        //disable layout
        $controller->view
                ->layout()
                ->disableLayout();

        //disable view auto-rendering
        $viewRenderer = $controller->getHelper('viewRenderer');
        if ($viewRenderer != null) {
            $viewRenderer->setNoRender();
        }

        //send response to client
        return $controller->result;
    }

    /**
     * Strategy pattern: call helper as broker method
     */
    public function direct() {
        return $this->renderAjaxResponse();
    }

}
