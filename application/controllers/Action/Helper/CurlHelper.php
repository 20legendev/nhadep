<?php
/**
 *
 * @author griever
 * @version 
 */
require_once 'Base.php';

/**
 * Encode Action Helper 
 * 
 * @uses actionHelper Custom_Helper
 */
class Custom_Helper_CurlHelper extends Dummy_Helper_Base {
	
	public function download($url) {
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		return curl_exec($ch);
	}
	
	/**
	 * Strategy pattern: call helper as broker method
	 */
	public function direct($url) {
		return $this->download($url);
	}
}

