<?php
/**
 *
 * @author griever
 * @version
 */
require_once 'controllers/BaseController.php';
require_once 'Zend/Loader/PluginLoader.php';
require_once 'Zend/Controller/Action/Helper/Abstract.php';

/**
 * Base Action Helper
 *
 * @uses actionHelper Dummy_Helper
 */
class Dummy_Helper_Base extends Zend_Controller_Action_Helper_Abstract
{
	/**
	 * @var Zend_Loader_PluginLoader
	 */
	public $pluginLoader;

	/**
	 * Constructor: initialize plugin loader
	 *
	 * @return void
	 */
	public function __construct(){
		// TODO Auto-generated Constructor
		$this->pluginLoader = new Zend_Loader_PluginLoader();
	}
	
	public function __get($index)	{
		return $this->getActionController()->$index;
	}
	
	public function getRootDir()	{
		return $this->getActionController()->getRootDir();
	}

	/**
	 * Strategy pattern: call helper as broker method
	 */
	public function direct(){
		// TODO Auto-generated 'direct' method
	}
	
	public function hasInputParam($param)	{
		return $this->getActionController()->hasInputParam($param);
	}
	
	public function _hasParam($param)	{
		return null !== $this->getRequest()->getParam($param);
	}
	
	public function getRequest()	{
		return $this->getActionController()->getRequest();
	}
}

