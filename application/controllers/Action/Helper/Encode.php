<?php
/**
 *
 * @author griever
 * @version 
 */
require_once 'Zend/Loader/PluginLoader.php';
require_once 'Base.php';
require_once 'CommonUtils.php';

/**
 * Encode Action Helper 
 * 
 * @uses actionHelper Custom_Helper
 */
class Custom_Helper_Encode extends Dummy_Helper_Base {
	
	public function encode($string, $root) {
		$encode_type = 'json';
		if ($this->_hasParam ( 'encode-type' )) {
			$encode_type = $this->getRequest ()->getParam ( 'encode-type' );
		}
		switch ($encode_type) 
		{
			case 'xml':
				header ("Content-Type:text/xml");
				$response = CommonUtils::encodeXML($string, $root);
				break;
			default:
				$response = CommonUtils::encodeJSON($string);
		}
		return $response;
	}
	
	/**
	 * Strategy pattern: call helper as broker method
	 */
	public function direct($string, $root) {
		return $this->encode($string, $root);
	}
}

