<?php
/**
 *
 * @author griever
 * @version 
 */
require_once 'Base.php';

/**
 * Encode Action Helper 
 * 
 * @uses actionHelper Custom_Helper
 */
class Custom_Helper_SendMail extends Dummy_Helper_Base {
	
	public function send($htmlPage, $from, $to, $subject, $params) {
		foreach($params as $key=>$value)	{
			$this->view->$key = $value;
		}
		$content = $this->view->render('email/'.$htmlPage.'.phtml');		
		$mail = new Zend_Mail('utf-8');
		$mail->setBodyHTML($content);
		$mail->setFrom($from['email'], $from['username']);
		foreach ($to as $user) {
			$mail->addTo($user['email'], $user['username']);
		}
		$mail->setSubject($subject);
		$mail->send();
		return true;
	}
	
	/**
	 * Strategy pattern: call helper as broker method
	 */
	public function direct($htmlPage, $from, $to, $subject, $params) {
		return $this->send($htmlPage, $from, $to, $subject, $params);
	}
}

