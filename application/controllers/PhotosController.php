<?php

require_once "Base/BaseWebController.php";
require_once 'DBUtils.php';
require_once 'CommonUtils.php';
require_once 'models/Images.php';
require_once 'models/ImagesTag.php';

class PhotosController extends BaseWebController {
	
	public function init(){
		$this->_helper->layout()->setLayout('layout');
		parent::init();
	}
	
	public function indexAction(){
		$images = new Images();
		$tags = new ImagesTag();
		$ret = $images->getAll();
		$this->view->total = $images->countAll();
		$this->view->images = $ret;
	}
	
	public function roomAction(){
		$images = new Images();
		$tags = new ImagesTag();
		$images->category = $this->_getParam('cat',1);
		$ret = $images->getByCategory();
		$count = $images->countByCategory();
		$this->view->images = $ret;
		$this->view->total = $count;
	}
	
}
