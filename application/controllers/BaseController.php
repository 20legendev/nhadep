<?php

abstract class BaseController extends Zend_Controller_Action {

    protected $user;
    protected $permitted;
    protected $browserCache = true;
	protected $userId;
    public $result;

    public function purify() {
    	
    }
	
    protected function purifyElement($purifier, $value) {
        if (is_object($value)) {
            return $value;
        }
        if (is_array($value)) {
            foreach ($value as $key => $v) {
                $value [$key] = $this->purifyElement($purifier, $v);
            }
        } else {
            $value = $purifier->purify($value);
        }
        return $value;
    }

    public function init() {
        $this->purify();
        $plugin = $this->getFrontController()->getPlugin(
                'Zend_Controller_Plugin_ErrorHandler');
        if ($plugin != null) {
            $plugin->setErrorHandler(
                    array('controller' => 'ajax-error', 'action' => 'error'));
        }
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
    }
    
    public function getUserId() {
		return $this->userId;
    }
	
	public function setLoggedIn($id) {
		$_SESSION['userid'] = $id;
		$this->userId = $id;
	}

    public function preDispatch() {
		require_once('models/Token.php');
        $userId = null;
		if (isset($_SESSION['userid'])) {
			$this->userId = $_SESSION['userid'];
		} else {
			$tokenParam = $this -> _getParam('token');
			if ($tokenParam !== NULL && $tokenParam !== '') {
				$token = new Token();
				$token->token = $tokenParam;
				$this->userId = $token->getByToken();
				$_SESSION['userid'] = $this->userId;
			}
		}
    }

	public function checkPermission($throws = true) {
        $this->permitted = false;
        if ($this->getUser() != null) {
            $permit = $this->permit($this->getUser());
        } else {
            $permit = $this->defaultPermission();
        }
        if (!$permit) {
            $this->permitted = false;
            if ($throws) {
                require_once ('Exceptions/PermissionDeniedException.php');
                throw new PermissionDeniedException ();
            } else {
                $this->_forward('login', 'user', null, array('error' => "You don't have permission to take this action. Please login"));
            }
            return false;
        }
        $this->permitted = true;
        return true;
	}

    public function permit($user) {
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
        $permission = new Permission(null);
        return $permission->check($user->role, $controller, $action);
    }

    public function defaultPermission() {
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
        $permission = new Permission(null);
        return $permission->check('default', $controller, $action);
    }

    public function userHasLogin() {
        return ($this->userId != null);
    }

    public function postDispatch() {
    	header('Access-Control-Allow-Origin: *');
        echo json_encode($this->result);
    }
}

?>
