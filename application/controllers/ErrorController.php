<?php 
	require_once 'BaseController.php';
	class ErrorController extends BaseController {
		
		public function errorAction() {
			echo $this->_getParam('error_handler')->exception->getMessage();
		}
	}
?>