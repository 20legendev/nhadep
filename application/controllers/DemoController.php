<?php

require_once "BaseController.php";
require_once 'DBUtils.php';
require_once 'CommonUtils.php';
require_once 'models/Demo.php';

class DemoController extends BaseController {
	
	public function indexAction(){
		$type = $this->_getParam('type',1);
		$arr = array();
		$demo = new Demo();
		for($i=1; $i < 6; $i++){
			$demo->type = $i;
			$arr[$i] = $demo->getByType();
		};
		
		$this->result = array(
			'data' => $arr,
			'status'=> 0,
			'msg'=> 'SUCCESS'
		);
	}
	
	public function saveAction(){
		if(!$_SESSION['userid']){
			die('MAY DI CHET DI');
		}
		$demo = new Demo();
		$demo->id = $this->_getParam('id');
		$demo->name = $this->_getParam('name');
		$demo->description = $this->_getParam('description');
		$demo->img = $this->_getParam('img');
		$demo->type = intval($this->_getParam('type'));
		$demo->link = $this->_getParam('link');
		$demo->tech = $this->_getParam('tech');
		$demo->platformtech = $this->_getParam('platformtech');
		$demo->industry = $this->_getParam('industry');
		$demo->sortidx = $this->_getParam('sortidx');
		
		$ret = $demo->select('id = ?id');
		if(count($ret) > 0){
			$demo->update('name,img,type,tech,platformtech,industry,description,link,sortidx');
		}else{
			$demo->insert('name,img,type,tech,platformtech,industry,description,link,sortidx');
		}
		$this->result = array(
			'status'=>0
		);
	}
	
}
