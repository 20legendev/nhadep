<?php

require_once "BaseController.php";
require_once 'DBUtils.php';
require_once 'CommonUtils.php';
require_once 'models/AdminImageUploaded.php';
require_once 'models/Article.php';

class AdminServiceController extends BaseController {
	
	public function loadMyImageAction(){
		if(!$this->userId){
			$this->result = array(
				'status'=>1,
				'msg'=>'NOT_LOGGED_IN'
			);
			return;
		}
		$imgUploaded = new AdminImageUploaded();
		$imgUploaded->user_id = $this->userId;
		$this->result = array(
			'status'=>0,
			'data'=>$imgUploaded->getByUserId(),
			'msg'=>'SUCCESS'
		);
	}
	
	public function uploadImageAction() {
		require_once 'models/UploadLogic.php';
		$allowedExtensions = array("jpg","png");
		$sizeLimit = 20 * 1024 * 1024;
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader -> handleUpload('./public/media/');
		if($result['success']){
			$imgUploaded = new AdminImageUploaded();
			$imgUploaded->link = $result['link'];
			$imgUploaded->user_id = $this->userId ? $this->userId : 1;
		  	$imgUploaded->insert('link,user_id');
		}
		$this->result = $result;
	}
	
	public function addArticleAction(){
		$id = $_SESSION['editid'];
		$article = new Article();
		if($id > 0){
			$article->id = $id;
			$article->load();
			$article->name = $this->_getParam('title');
			$article->description = $this->_getParam('shortContent');
			$article->content = $this->_getParam('start');
			$article->update('name,description,content', 'id = ' . $id);
			$type = 1;
		}else{
			$ret = $article->insert('name,description,content');
			$type = 0;	
		}
		
		$images = $this->_getParam('images');
		require_once 'models/ArticleImage.php';
		$ai = new ArticleImage();
		if($type == 0){
			$ai->article_id = $article->getLastInsertId();
		}else{
			$ai->article_id = $id;
			$data = $ai->getByArticle();
			for($i=0;$i<count($data); $i++){
				$data[$i]->delete('id = ?id');
			} 
		}
		for($i=0;$i<count($images); $i++){
			$ai->image_link = $images[$i]['image'];
			$ai->image_desc = $images[$i]['content'];
			$ai->insert('article_id,image_link,image_desc');
		}
		$this->result = array(
			'status'=>0,
			'msg'=>'SUCCESS'
		);
	}
}
