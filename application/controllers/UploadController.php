<?php

require_once "BaseController.php";
require_once 'DBUtils.php';

class UploadController extends BaseController {

	public function indexAction() {
		$this->result = array('result'=>'hello');
	}

	public function imageAction() {
		require_once 'models/UploadLogic.php';
		$allowedExtensions = array("jpg","png");
		$sizeLimit = 20 * 1024 * 1024;
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader -> handleUpload('./public/media/');
		$this->result = $result;
	}
}
