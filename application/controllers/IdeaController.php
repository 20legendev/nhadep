<?php

require_once "Base/BaseWebController.php";
require_once 'DBUtils.php';
require_once 'CommonUtils.php';
require_once 'models/Article.php';
require_once 'models/ArticleImage.php';

class IdeaController extends BaseWebController {
	
	public function init(){
		$this->_helper->layout()->setLayout('layout');
		parent::init();
	}
	
	public function indexAction(){
		$article = new Article();
		$data = $article->getByPage(0, 7);
		$ai = new ArticleImage();
		for($i=0;$i<count($data);$i++){
			$ai->article_id = $data[$i]->id;
			$data[$i]->images = $ai->getByArticle();
		} 
		$this->view->articles = $data;
	}
	
	public function detailAction(){
		$id = $this->_getParam('id');
		$article = new Article();
		$article->id = $id;
		if($article->load()){
			$ai = new ArticleImage();
			$ai->article_id = $id;
			$article->images = $ai->getByArticle();
		}
		$this->view->article = $article;
		
		/* relate product */
		$article = new Article();
		$article->id = $id;
		$this->view->related = $article->getRelate();
		
		/* latest */
		$this->view->latest = $article->getLatest();
	}
	
	
}
