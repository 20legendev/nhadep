<?php
require_once('Ice/Db/Table.php');

class ArticleImage extends Ice_Db_Table
{
    public $id;
	public $artile_id;
	public $image_link;
	public $image_desc;
	
    public function __construct(){
    	$this->key = 'id';
        $this->table = 'article_image';
        parent::__construct();
    }
	
	public function getByArticle(){
		return $this->select('article_id = ?article_id');
	}
	
}
?>