<?php
require_once('Ice/Db/Table.php');

class Token extends Ice_Db_Table
{
    public $id;
	public $user_id;
	public $token;
	
    public function __construct(){
    	$this->key = 'id';
        $this->table = 'token';
        parent::__construct();
    }
    
    public function add() {
    	return $this->insert('token, user_id');
    }
	
	public function remove() {
		return $this->delete('token = ?token AND user_id = ?user_id');
	}
	
	public function getByToken() {
		$result = $this->select('token = ?token');
		if (count($result) > 0) {
			return $result[0]->user_id;
		}
		return null;
	}
}
?>