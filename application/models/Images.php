<?php
require_once('Ice/Db/Table.php');

class Images extends Ice_Db_Table
{
    public $id;
	public $userid;
	public $title;
	public $link;
	public $description;
	public $category;
	public $view;
	public $like;
	public $since;
	
    public function __construct(){
    	$this->key = 'id';
        $this->table = 'images';
        parent::__construct();
    }
	
	public function getAll(){
		$query = 'SELECT * FROM (images INNER JOIN user on images.userid = user.id) ORDER BY images.id desc';
		return $this->query($query);
	}
	
	public function getByCategory(){
		$query = 'SELECT * FROM (images INNER JOIN user on images.userid = user.id AND images.category = '.$this->category.') ORDER BY images.id desc';
		return $this->query($query);
	}
	
	public function countByCategory(){
		return count($this->select('category = ?category'));
	}
	
	public function countAll(){
		return count($this->select());
	}
	
}
?>