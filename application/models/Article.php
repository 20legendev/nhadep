<?php
require_once('Ice/Db/Table.php');

class Article extends Ice_Db_Table
{
    public $id;
	public $name;
	public $description;
	public $content;
	public $userid;
	public $since;
	public $hit;
	public $comment;
	
    public function __construct(){
    	$this->key = 'id';
        $this->table = 'article';
        parent::__construct();
    }
	
	public function getAll(){
		return $this->select('1=1',NULL,'id desc');
	}
	
	public function getByPage($page, $pageSize){
		return $this->select('1=1',NULL,'id desc', NULL, $page, $pageSize);
	}
	
	public function getLatest(){
		$query = 'SELECT article.id,article.name,article_image.image_link FROM (article INNER JOIN article_image ON article.id = article_image.article_id) ';
		$query .= ' GROUP BY article.id ORDER BY article.id DESC LIMIT 10';
		echo $query;
		return $this->query($query);
	}
	
	public function getLastInsertId(){
		return $this->adapter->getLastInsertId();
	}
	
	public function getRelate(){
		$query = 'SELECT article.id,article.name,article_image.image_link FROM (article INNER JOIN article_image ON article.id = article_image.article_id ';
		$query .= ' AND article.id < '.$this->id.') GROUP BY article.id ORDER BY article.id DESC LIMIT 10';
		return $this->query($query);
	}
	
}
?>