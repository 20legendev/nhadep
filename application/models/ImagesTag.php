<?php
require_once('Ice/Db/Table.php');

class ImagesTag extends Ice_Db_Table
{
    public $id;
	public $imageid;
	public $tagid;

    public function __construct(){
    	$this->key = 'imageid';
        $this->table = 'imagetag';
        parent::__construct();
    }
	
	public function getTagByImages($imageid){
		$query = 'SELECT tag.id, tag.name FROM (tag INNER JOIN imagetag ON tag.id = imagetag.tagid '.
				'INNER JOIN images ON images.id = imagetag.imageid '.
				'AND images.id = '.$imageid. ')';
		return $this->query($query);
	}
}
?>