<?php
require_once('Ice/Db/Table.php');

class AdminImageUploaded extends Ice_Db_Table
{
    public $id;
	public $user_id;
	public $link;
	
    public function __construct(){
    	$this->key = 'id';
        $this->table = 'admin_imageupload';
        parent::__construct();
    }
	
	public function getByUserId(){
		return $this->select('user_id = ?user_id', NULL, 'id desc');
	}
}
?>